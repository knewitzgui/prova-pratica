using NUnit.Framework;
using ProvaPratica;

namespace Tests
{
    public class Tests
    {
        [Test]
        public void TesteCPF()
        {
            string documento = "06871073930";
            bool result = true;
            Assert.AreEqual(result, Validador.CPF(documento));
        }
        [Test]
        public void TesteCNPJ()
        {
            string documento = "28.666.540/0001-28";
            bool result = true;
            Assert.AreEqual(result, Validador.CNPJ(documento));
        }
    }
}